# PostGIS

This image provides a PostGIS database based on the
[centos/postgresql-12-centos7](https://hub.docker.com/r/centos/postgresql-12-centos7)
image for compatibility with Openshift. Please refer to the
[centos/postgresql-12-centos7 documentation](https://hub.docker.com/r/centos/postgresql-12-centos7)
for the available configuration options.

## Environment variables and volumes

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker run command.

**`POSTGRESQL_USER`**
       User name for PostgreSQL account to be created

**`POSTGRESQL_PASSWORD`**
       Password for the user account

**`POSTGRESQL_DATABASE`**
       Database name

**`POSTGRESQL_ADMIN_PASSWORD`**
       Password for the `postgres` admin account (optional)


Alternatively, the following options are related to migration scenario:

**`POSTGRESQL_MIGRATION_REMOTE_HOST`**
       Hostname/IP to migrate from

**`POSTGRESQL_MIGRATION_ADMIN_PASSWORD`**
       Password for the remote 'postgres' admin user

**`POSTGRESQL_MIGRATION_IGNORE_ERRORS (optional, default 'no')`**
       Set to 'yes' to ignore sql import errors


The following environment variables influence the PostgreSQL configuration file. They are all optional.

**`POSTGRESQL_MAX_CONNECTIONS (default: 100)`**
       The maximum number of client connections allowed

**`POSTGRESQL_MAX_PREPARED_TRANSACTIONS (default: 0)`**
       Sets the maximum number of transactions that can be in the "prepared" state. If you are using prepared transactions, you will probably want this to be at least as large as max_connections

**`POSTGRESQL_SHARED_BUFFERS (default: 32M)`**
       Sets how much memory is dedicated to PostgreSQL to use for caching data

**`POSTGRESQL_EFFECTIVE_CACHE_SIZE (default: 128M)`**
       Set to an estimate of how much memory is available for disk caching by the operating system and within the database itself


You can also set the following mount points by passing the `-v /host/dir:/container/dir:Z` flag to Docker.

**`/var/lib/pgsql/data`**
       PostgreSQL database cluster directory


**Notice: When mouting a directory from the host into the container, ensure that the mounted
directory has the appropriate permissions and that the owner and group of the directory
matches the user UID or name which is running inside the container.**

Typically (unless you use `podman run -u` option) processes in container
run under UID 26, so -- on GNU/Linux -- you can fix the datadir permissions
for example by:

```
$ setfacl -m u:26:-wx /your/data/dir
$ podman run <...> -v /your/data/dir:/var/lib/pgsql/data:Z <...>
```

## Restore on start

You can copy files into the directory `/restore` to be restored
on each container start up.

```dockerfile
COPY --chown=26:0 path/to/my/data.dump /restore/
```

You can copy multiple files into this folder. They will be processed
in alphabetical order. Files with the ending
***.sql** will be executed using **psql**. All other files will be
handled using **pg_restore**.

By default, each file will be deleted after restore. If you want to
keep them, you have to set `REMOVE_AFTER_RESTORE=false`. This might be
useful, if you mount a persistent volume at `/restore` which is used
by other containers too.

> **WARNING:** Do not use this option if you are using a persistent
volume for your database. The restore script is called on each start
of the container, not only on database initialization. This could
cause existing data to be overwritten as **pg_restore** is called
with the **--clean** and **--if-exists** options.
