#!/usr/bin/env bash
psql -U postgres -d ${POSTGRESQL_DATABASE} -c "CREATE EXTENSION IF NOT EXISTS postgis;"
psql -U postgres -d ${POSTGRESQL_DATABASE} -c "CREATE EXTENSION IF NOT EXISTS postgis_raster;"
psql -U postgres -d ${POSTGRESQL_DATABASE} -c "CREATE EXTENSION IF NOT EXISTS postgis_topology;"
psql -U postgres -d ${POSTGRESQL_DATABASE} -c 'CREATE EXTENSION IF NOT EXISTS "uuid-ossp";'
