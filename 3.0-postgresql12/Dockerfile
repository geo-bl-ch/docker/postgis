# -------------
# Builder image
# -------------

FROM centos/postgresql-12-centos7 AS builder

USER 0

# Install runtime dependencies
RUN yum -y upgrade && \
    yum -y install \
        zlib \
        libtiff \
        libstdc++ \
        libxml2 \
        json-c \
        perl && \
    yum clean all && \
    rm -rf /tmp/* && \
    rm -rf /user/local/man

# Install dev dependencies
RUN yum -y upgrade && \
    yum -y install \
        which \
        wget \
        gcc \
        gcc-c++ \
        make \
        file \
        unzip \
        git \
        autoconf \
        automake \
        libtool \
        zlib-devel \
        libtiff-devel \
        rh-postgresql12-postgresql-devel \
        libxml2-devel \
        json-c-devel

# Install SQLite
RUN cd /tmp && \
    wget --no-check-certificate https://www.sqlite.org/2020/sqlite-autoconf-3310000.tar.gz && \
    tar xfz sqlite-autoconf-3310000.tar.gz && \
    rm -f sqlite-autoconf-3310000.tar.gz && \
    cd sqlite-autoconf-3310000 && \
    ./configure && \
    make --jobs=$(nproc) --quiet && \
    make --jobs=$(nproc) --quiet install && \
    ldconfig

# Install proj
RUN cd /tmp && \
    wget -q http://download.osgeo.org/proj/proj-6.3.0.tar.gz && \
    tar xzf proj-6.3.0.tar.gz && \
    rm -f proj-6.3.0.tar.gz && \
    wget -q http://download.osgeo.org/proj/proj-datumgrid-1.8.zip && \
    unzip -q -o proj-datumgrid-1.8.zip -d proj-6.3.0/data/ && \
    rm -f proj-datumgrid-1.8.zip && \
    wget -q http://download.osgeo.org/proj/proj-datumgrid-europe-1.5.zip && \
    unzip -q -o proj-datumgrid-europe-1.5.zip -d proj-6.3.0/data/ && \
    rm -f proj-datumgrid-europe-1.5.zip && \
    cd proj-6.3.0 && \
    export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/local/lib/pkgconfig && \
    ./configure && \
    make --jobs=$(nproc) --quiet && \
    make --jobs=$(nproc) --quiet install && \
    ldconfig

# Install GeoTIFF
RUN cd /tmp && \
    wget http://download.osgeo.org/geotiff/libgeotiff/libgeotiff-1.5.1.tar.gz && \
    tar xfz libgeotiff-1.5.1.tar.gz  && \
    rm -f libgeotiff-1.5.1.tar.gz && \
    cd libgeotiff-1.5.1 && \
    ./configure --with-jpeg=yes --with-zlib=yes --with-proj=/usr/local && \
    make --jobs=$(nproc) --quiet && \
    make --jobs=$(nproc) --quiet install && \
    ldconfig

# Install geos
RUN cd /tmp && \
    git clone https://git.osgeo.org/gitea/geos/geos.git geos && \
    cd geos && \
    git checkout 3.8.0 && \
    ./autogen.sh && \
    mkdir obj && \
    cd obj && \
    ../configure && \
    make --jobs=$(nproc) --quiet && \
    make --jobs=$(nproc) --quiet install && \
    ldconfig

# Install GDAL
RUN cd /tmp && \
    wget https://github.com/OSGeo/gdal/releases/download/v3.0.3/gdal-3.0.3.tar.gz && \
    tar xzf gdal-3.0.3.tar.gz && \
    rm -f gdal-3.0.3.tar.gz && \
    cd gdal-3.0.3 && \
    LDFLAGS='-L/opt/rh/rh-postgresql12/root/lib64/ -L/opt/rh/rh-postgresql12/root/lib64/pgsql/' \
    ./configure \
        --with-geotiff=/usr/local \
        --with-proj=/usr/local \
        --with-pg=/opt/rh/rh-postgresql12/root/usr/bin/pg_config \
        --with-geos=/usr/local/bin/geos-config && \
    make --jobs=$(nproc) --quiet && \
    make --jobs=$(nproc) --quiet install && \
    ldconfig

# Install PostGIS
RUN cd /tmp && \
    wget http://download.osgeo.org/postgis/source/postgis-3.0.0.tar.gz && \
    tar xzf postgis-3.0.0.tar.gz && \
    rm -f postgis-3.0.0.tar.gz && \
    cd postgis-3.0.0 && \
    ./configure \
        --with-library-minor-version \
        --with-pgconfig=/opt/rh/rh-postgresql12/root/usr/bin/pg_config \
        --with-geosconfig=/usr/local/bin/geos-config \
        --with-gdalconfig=/usr/local/bin/gdal-config && \
    make --jobs=$(nproc) --quiet && \
    make --jobs=$(nproc) --quiet install && \
    ldconfig

# -------------
# Runtime image
# -------------

FROM centos/postgresql-12-centos7
LABEL maintainer="Karsten Deininger <karsten.deininger@bl.ch>"

# Switch to root user for installing dependencies
USER 0

# Install runtime dependencies
RUN yum -y upgrade && \
    yum -y install \
        zlib \
        libtiff \
        libstdc++ \
        libxml2 \
        json-c \
        perl && \
    yum clean all && \
    rm -rf /tmp/* && \
    rm -rf /user/local/man

# Copy libraries and binaries
COPY --from=builder /usr/local/lib/libgeos* /opt/rh/rh-postgresql12/root/usr/lib64/
COPY --from=builder /usr/local/lib/libgdal* /opt/rh/rh-postgresql12/root/usr/lib64/
COPY --from=builder /usr/local/lib/libgeotiff* /opt/rh/rh-postgresql12/root/usr/lib64/
COPY --from=builder /usr/local/lib/libproj* /opt/rh/rh-postgresql12/root/usr/lib64/
COPY --from=builder /usr/local/lib/libsqlite3* /opt/rh/rh-postgresql12/root/usr/lib64/
COPY --from=builder /usr/lib64/libsqlite3* /opt/rh/rh-postgresql12/root/usr/lib64/
COPY --from=builder /usr/lib64/libsqlite3* /opt/rh/rh-postgresql12/root/usr/lib64/
COPY --from=builder /usr/local/bin/* /opt/rh/rh-postgresql12/root/usr/bin/
COPY --from=builder /opt/rh/rh-postgresql12/root/usr/share/pgsql/ /opt/rh/rh-postgresql12/root/usr/share/pgsql/
COPY --from=builder /opt/rh/rh-postgresql12/root/usr/lib64/ /opt/rh/rh-postgresql12/root/usr/lib64/

RUN ldconfig

# Create restore folder
RUN mkdir /restore && \
    chown 26:0 /restore && \
    chmod g=u /restore

# Set default environment variables
ENV REMOVE_AFTER_RESTORE=true

# Copy start up scripts
COPY --chown=26:0 postgresql-start/ /opt/app-root/src/postgresql-start/
COPY is_ready /usr/local/bin/

# Switch back to unpriviledged user
USER 26
