#!/usr/bin/env bash

set -e

files=$(find /restore -name '*.*' | sort)

if [[ -n $files ]]
then
    echo "data to be restored:"
    for file in $files; do
        echo "    $file"
    done
else
    echo "no data to be restored"
fi

for file in $files; do

    # Check how to handle the file (psql/pg_restore)
    if [[ $file == *.sql ]]
    then
        echo "restoring $file using psql..."
        psql -U postgres -d ${POSTGRESQL_DATABASE} -f $file
        echo "finished restoring $file"
    else
        echo "restoring $file using pg_restore..."
        pg_restore -U postgres -d ${POSTGRESQL_DATABASE} --clean --if-exists --no-owner $file
        echo "finished restoring $file"
    fi

    # Optionally remove file after restore
    if "$REMOVE_AFTER_RESTORE"
    then
        echo "removing $file..."
        rm -f $file
    fi

done

touch /tmp/ready
